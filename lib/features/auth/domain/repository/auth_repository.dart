import 'package:dartz/dartz.dart';
import 'package:restaurant/core/error/failures.dart';
import 'package:restaurant/features/auth/domain/entity/user.dart';

abstract class AuthRepository {
  Future<Either<Failure,User>> login(String user, String pass);
  Future<Either<Failure,User>> chefLogin(String user, String pass);
}
