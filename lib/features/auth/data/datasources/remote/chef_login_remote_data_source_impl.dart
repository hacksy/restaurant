import 'package:dio/dio.dart';
import 'package:restaurant/core/error/exceptions.dart';
import 'package:restaurant/features/auth/data/models/user_model.dart';
import 'package:restaurant/features/auth/data/models/user_request.dart';

class ChefLoginRemoteDataSourceImpl extends ChefLoginRemoteDataSource {
  ChefLoginRemoteDataSourceImpl({
    this.dio,
  });
  final Dio dio;
  @override
  Future<UserModel> getChef(UserRequest user) async {
    try {
      final response = await dio.post(
        'https://restaurant-administrador-am.herokuapp.com/auth',
        options: Options(
          headers: {'Authorization': user.toBasicAuth()},
        ),
      );
      if (response.statusCode == 200) {
        return response.data;
      }
      throw ServerException();
    } on DioError {
      throw ServerException();
    }
  }
}

abstract class ChefLoginRemoteDataSource {
  Future<UserModel> getChef(UserRequest user);
}
