import 'package:dio/dio.dart';

class DioTokenInterceptor extends Interceptor {
  String token = '';
  @override
  Future onResponse(Response response) async {
    if (response.data != null && response.data.isNotEmpty) {
      if (response.data is Map && response.data.containsKey('token')) {
        token = response.data['token'];
      }
    }
    return response;
  }

  @override
  Future onRequest(RequestOptions request) async {
    if (!request.headers.containsKey('Authorization')) {
      if (token != null && token.isNotEmpty) {
        request.headers.addAll({'Authorization': token});
      }
    }
    return request;
  }

  @override
  Future onError(DioError err) async {
    if (err.response.statusCode == 401) {
      token = '';
    }
  }
}
