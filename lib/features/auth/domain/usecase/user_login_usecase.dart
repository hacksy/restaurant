import 'package:equatable/equatable.dart';
import 'package:restaurant/core/error/failures.dart';
import 'package:dartz/dartz.dart';
import 'package:restaurant/core/usecase/usecase.dart';
import 'package:restaurant/features/auth/domain/entity/user.dart';
import 'package:restaurant/features/auth/domain/repository/auth_repository.dart';

class UserLoginUseCase implements UseCase<User, UserParams> {
  final AuthRepository repository;
  UserLoginUseCase(this.repository);
  @override
  Future<Either<Failure, User>> call(UserParams params) async{
    return await repository.login(params.user, params.pass);
  }
}

class UserParams extends Equatable {
  final String user;
  final String pass;
  UserParams(this.user, this.pass);
  @override
  List<Object> get props => [user, pass];
}
