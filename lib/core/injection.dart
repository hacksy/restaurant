import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:restaurant/core/interceptors/dio_token_interceptor.dart';
import 'package:restaurant/features/auth/data/datasources/remote/chef_login_remote_data_source_impl.dart';
import 'package:restaurant/features/auth/data/datasources/remote/user_login_remote_data_source.dart';
import 'package:restaurant/features/auth/data/repository/auth_repository_impl.dart';
import 'package:restaurant/features/auth/domain/repository/auth_repository.dart';
import 'package:dio_cache_interceptor/dio_cache_interceptor.dart';
import 'package:restaurant/features/auth/domain/usecase/user_login_usecase.dart';
import 'package:restaurant/features/auth/presentation/provider/auth/auth_model.dart';

final sl = GetIt.instance;

void setupGetIt() async {
  sl.registerSingleton(DioTokenInterceptor());
  initHttpClient();
  sl.registerSingleton<ChefLoginRemoteDataSource>(ChefLoginRemoteDataSourceImpl(
    dio: sl(),
  ));
  sl.registerSingleton<UserLoginRemoteDataSource>(UserLoginRemoteDataSourceImpl(
    dio: sl(),
  ));
  sl.registerSingleton<AuthRepository>(AuthRepositoryImpl(
    userLoginRemoteDataSource: sl(),
    chefLoginRemoteDataSource: sl(),
  ));
  sl.registerSingleton(UserLoginUseCase(sl()));
  sl.registerSingleton(AuthModel(sl()));
}

void initHttpClient() {
  final options = CacheOptions(
    store: DbCacheStore(), // Required.
    policy: CachePolicy.cacheFirst,
    hitCacheOnErrorExcept: [401, 403],
    priority: CachePriority.normal,
    maxStale: const Duration(days: 7),
  );

  sl.registerSingleton(
    Dio()
      ..interceptors.add(sl<DioTokenInterceptor>())
      ..interceptors.add(DioCacheInterceptor(options: options))
      ..interceptors.add(PrettyDioLogger(request: true,requestBody:true, requestHeader:true))
  );
}
