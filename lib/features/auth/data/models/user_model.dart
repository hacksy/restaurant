import 'package:meta/meta.dart';
import 'package:restaurant/features/auth/domain/entity/user.dart';

class UserModel extends User {
  UserModel({
    @required String token,
    String id,
    UserKind kind = UserKind.user,
  }) : super(token: token, id: id, kind: kind);

  factory UserModel.fromJson(
    Map<String, dynamic> json, {
    bool isChef = false,
  }) {
    return UserModel(
      token: json['token'],
      id: json['id'] ?? '',
      kind: isChef ? UserKind.chef : UserKind.user,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'token': token,
      'id': id,
    };
  }
}
