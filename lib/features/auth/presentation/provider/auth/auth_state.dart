import 'package:equatable/equatable.dart';
import 'package:restaurant/features/auth/domain/entity/user.dart';

enum AuthStatus {
  initial,
  loading,
  logged_in,
  logged_out,
  error,
}

class AuthState extends Equatable{
  final AuthStatus status;
  final User user;
  const AuthState._({this.user, this.status});

  const AuthState.initial() : this._(status: AuthStatus.initial);
  const AuthState.loading() : this._(status: AuthStatus.loading);
  const AuthState.loggedIn(User user)
      : this._(status: AuthStatus.logged_in, user: user);
  const AuthState.loggedOut() : this._(status: AuthStatus.logged_out);
  const AuthState.error() : this._(status: AuthStatus.error);
  @override
  List<Object> get props => [status, user];


}
