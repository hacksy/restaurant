import 'package:flutter/material.dart';
import 'package:restaurant/core/injection.dart';
import 'package:restaurant/features/auth/presentation/provider/auth/auth_state.dart';
import 'package:restaurant/features/home/presentation/home_page.dart';

import 'provider/auth/auth_model.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String _username = '';
  String _password = '';
  @override
  void initState() {
    sl<AuthModel>().addListener(() {
      if (sl<AuthModel>().authState.status == AuthStatus.logged_in) {
        Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          return HomePage();
        }));
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            TextField(
              onChanged: (string) {
                _username = string;
              },
            ),
            TextField(
              obscureText: true,
              onChanged: (string) {
                _password = string;
              },
            ),
            RaisedButton(
              onPressed: () {
                sl<AuthModel>().login(_username, _password);
              },
              child: Text("Login"),
            ),
          ],
        ),
      ),
    );
  }
}
