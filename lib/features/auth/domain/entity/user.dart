import 'package:meta/meta.dart';

class User {
  final String token;
  final String id;
  final UserKind kind;
  User({
    @required this.token,
    this.id,
    this.kind = UserKind.user,
  });
}

enum UserKind {
  chef,
  user,
}
