import 'dart:convert';

import 'package:meta/meta.dart';

class UserRequest {
  final String user;
  final String pass;
  UserRequest({
    @required this.user,
    @required this.pass,
  })  : assert(user != null),
        assert(pass != null);
        
  String toBasicAuth() {
    final nonEncoded = base64Encode(utf8.encode('$user:$pass'));
    return 'Basic $nonEncoded';
  }

  Map<String, dynamic> toJson() {
    return {
      'user': user,
      'pass': pass,
    };
  }
}
