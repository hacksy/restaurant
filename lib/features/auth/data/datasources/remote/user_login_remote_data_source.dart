import 'package:dio/dio.dart';
import 'package:restaurant/core/error/exceptions.dart';
import 'package:restaurant/features/auth/data/models/user_model.dart';
import 'package:restaurant/features/auth/data/models/user_request.dart';

class UserLoginRemoteDataSourceImpl extends UserLoginRemoteDataSource {
  UserLoginRemoteDataSourceImpl({
    this.dio,
  });
  final Dio dio;
  @override
  Future<UserModel> getUser(UserRequest user) async {
    try {
      final response = await dio.post(
        'https://restaurant-administrador-am.herokuapp.com/auth-client',
        options: Options(
          headers: {'Authorization': user.toBasicAuth()},
        ),
      );

      if (response.statusCode == 200) {
       return UserModel.fromJson(response.data);
      }
      throw ServerException();
    } on DioError {
      throw ServerException();
    }
  }
}

abstract class UserLoginRemoteDataSource {
  Future<UserModel> getUser(UserRequest user);
}
