import 'package:dartz/dartz.dart';
import 'package:restaurant/core/error/exceptions.dart';
import 'package:restaurant/core/error/failures.dart';
import 'package:restaurant/features/auth/data/datasources/remote/chef_login_remote_data_source_impl.dart';
import 'package:restaurant/features/auth/data/datasources/remote/user_login_remote_data_source.dart';
import 'package:restaurant/features/auth/data/models/user_model.dart';
import 'package:restaurant/features/auth/data/models/user_request.dart';
import 'package:restaurant/features/auth/domain/entity/user.dart';
import 'package:restaurant/features/auth/domain/repository/auth_repository.dart';

typedef _UserModel = Future<UserModel> Function();

class AuthRepositoryImpl extends AuthRepository {
  AuthRepositoryImpl({
    this.userLoginRemoteDataSource,
    this.chefLoginRemoteDataSource,
  });
  final UserLoginRemoteDataSource userLoginRemoteDataSource;
  final ChefLoginRemoteDataSource chefLoginRemoteDataSource;
  @override
  Future<Either<Failure, User>> chefLogin(String user, String pass) {
    return _getData(() =>
        chefLoginRemoteDataSource.getChef(UserRequest(user: user, pass: pass)));
  }

  @override
  Future<Either<Failure, User>> login(String user, String pass) {
    return _getData(() =>
        userLoginRemoteDataSource.getUser(UserRequest(user: user, pass: pass)));
  }

  Future<Either<Failure, User>> _getData(_UserModel userModellCall) async {
    //TODO: Check if I have internet
    try {
      final userModel = await userModellCall();
      return Right(userModel);
    } on ServerException {
      return Left(ServerFailure());
    }
  }
}
