import 'package:flutter/widgets.dart';
import 'package:restaurant/core/error/failures.dart';
import 'package:restaurant/features/auth/domain/entity/user.dart';
import 'package:restaurant/features/auth/domain/usecase/user_login_usecase.dart';
import 'package:restaurant/features/auth/presentation/provider/auth/auth_state.dart';

class AuthModel extends ChangeNotifier {
  final UserLoginUseCase userLoginUseCase;
  AuthModel(this.userLoginUseCase);
  AuthState _authState = AuthState.initial();

  AuthState get authState => _authState;

  void login(String user, String pass) async {
    //Loading
    _authState = AuthState.loading();
    notifyListeners();
    //check user
    final userOrFailure = await userLoginUseCase.call(UserParams(user, pass));

    /*userOrFailure.fold((Failure failure) {
      _authState = AuthState.error();
      notifyListeners();
    }, (User _user) {
      _authState = AuthState.loggedIn(_user);
      notifyListeners();
    });*/
  }
}
